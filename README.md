## Using this code

#### RStudio setup
Assuming you are using RStudio before you run the EM algorithm (both for single promoters and all promoters) it would be good to:

* "clear objects from the workspace" so that no preexisting variables are present. This can be done in the "Environment" window on the top-right corner of RStudio.
* set the working directory at the top folder within the cloned repository (meaning the folder which contains the ".git" folder)

#### Main datasets

*  ampMethMatrix_relative2TSS.RDS (all promoters)
*  ampMethMatrixByPromoter_300bp.RDS (single promoters)

found in the "DataSets/RDS" directory, and

*  finalChosenList_tss_dc_tissues.csv (expression levels)

found in the "DataSets/ExpressionLevels" directory.

#### Preprocessing
These datasets are preprocessed by the code in the files:

* EM-PreprocessData_SingleGenes.R
* EM-PreprocessData_AllGenes.R

found in the "EM" directory. The processed datasets are stored as separate RDS files. This preprocessing step is required before the EM algorithm is run.

#### Running EM
The EM can be run by using the following files:

* EM-SinglePromoters.R
* EM-AllPromoters-Train.R
* EM-AllPromoters-Test.R

##### Single promoters
After preprocessing the data (EM-PreprocessData_SingleGenes.R) the EM can be run for "single" promoters. All the configuration can be set at the top of the "EM-SinglePromoters.R" file.

##### All promoters
After preprocessing the data (EM-PreprocessData_AllGenes.R) the EM can be run for "all" promoters. First configre the window sizes at the top of the "EM-PrepareWindows.R" file. Afterwards also configure "EM-AllPromoters-Train.R" at the top. This code is used to train new clusters for all genes. Optionally, after these clusters are trained, one can use the "EM-AllPromoters-Test.R" file to assign existing clusters to new data (make sure to preprocess and use other data than the data used for training).


