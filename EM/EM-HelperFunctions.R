############################### EM HELPERS #############################################
p_converged <- function(p_diff, p_diff_prev, error) {
  return(abs(p_diff-p_diff_prev) < error)
}

p_difference <- function(p, p_prev) {
  return(sum(abs(p-p_prev)))
}

order_by_prev_cluster <- function(prev_classMeans, classMeans) {
  pr = prev_classMeans
  ord = numeric(length = K)
  for (i in 1:K) {
    # compare cluster means by minimum sum of squares
    pos = which.min(apply(prev_classMeans, 1, function(row) sum((row-classMeans[i,])^2) ))
    ord[i] = pos
    prev_classMeans[pos,] = Inf
  }
  
  return(ord)
}
########################################################################################

############################### PREPARE WINDOWS HELPERS ################################
countInformativeInWindow <- function(window) {
  sum(!is.na(window))
}

countZerosInWindow <- function(window) {
  sum(window == 0, na.rm = T)
}
########################################################################################

############################### SILHOUETTE HELPERS #####################################
silhouetteSingleK <- function() {
  silK = silhouette(readsClasses, dis)
  sil[K] <- mean(silK[, 3])
  
  pdf(paste(dirEM_classMaps, gene, "_Silhouette_K", K, ".pdf", sep=""))
  plot(silK)
  abline(v=sil[K], col="red")
  dev.off()
  
  sil <<- sil
}

silhouetteAllKs <- function() {
  pdf(paste(dirEM_classMaps, gene, "_Silhouette_Ks.pdf", sep=""))
  # Plot the  average silhouette width
  plot(1:max(K_range), sil, type = "b", pch = 19, 
       frame = FALSE, 
       xlab = "Number of clusters K",
       ylab = "Average Silhouette width",
       main = paste(gene, ", Reads:", nrow(data), sep = ""))
  abline(v = which.max(sil), lty = 2)
  dev.off()
  
  # Merge all Silhouette PDFs
  # works on Linux if "pdfunite" is installed
  # system(paste("cd ",dirEM_classMaps, " && pdfunite ", gene , "* ", gene, "_all.pdf",sep=""))
}
########################################################################################

############################### PLOT PCA HELPERS #######################################
# Adapted from: https://stackoverflow.com/a/7963963/1007235
substrRight <- function(x, n){
  substr(x, nchar(x)-n+1, nchar(x))
}
########################################################################################