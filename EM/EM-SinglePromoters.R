# Adjusted from: https://academic.oup.com/bioinformatics/article/30/17/2406/2748187#supplementary-data

# Global paths
source(paste(getwd(), "/Global.R", sep=""))

# Prepare Windows
source(paste(getwd(), "/EM/EM-PrepareWindows.R", sep=""))

# Helper functions
source(paste(getwd(), "/EM/EM-HelperFunctions.R", sep=""))

# Plot image matrix
source(paste(getwd(), "/EM/EM-PlotClassMap.R", sep=""))

# For Silhouette
library(cluster)

# Configuration
K_range = 2:20                       # Number of classes to be found
maxEMIterations = 100
convergence_error = 10^-6
geneNr = 1
performSilhouette = T
silhouetteDistance = "euclidean"
####################################################################

# Data (all matrices for single promoters (single genes))
ampMethMatrixByPromoter = readRDS(paste(dirRDS, "ampMethMatrixByPromoterReducedOver50Shuffled.rds", sep = ""))

gene = names(ampMethMatrixByPromoter)[geneNr]
# Matrix containing the samples. data[i,j] is the bincount of sample i at position j.
data = ampMethMatrixByPromoter[[gene]]

# orders the clusters in the plot according to the first plot for easier visual comparison
orderByPreviousClusters = F
if (exists("firstClusteringClassMeans") && !is.null(firstClusteringClassMeans)) {
  orderByPreviousClusters = T
} else {
  firstClusteringClassMeans = vector(mode="list", length=length(K_range))
}

# Adjusted from: http://www.sthda.com/english/wiki/print.php?id=239#average-silhouette-method
# vector of Silhouette values for K determination
if (performSilhouette) {
  sil <- rep(0, max(K_range))
  dis = dist(data, method = silhouetteDistance) # might not work for >2000 reads (huge matrix)
}

###################### EM BASIC ###############################################
# c:    a matrix containing the classes to be optimized. c[i,j] is the expected
#       bincount value of class i at position j.
# q:    a vector defining the prior probabilities of each class.
# data: a matrix containing the samples. data[i,j] is the bincount of sample i at position j.
em_basic = function(c,q,data) {
  K=dim(c)[1]               # Number of classes
  N=dim(data)[1]            # Number of samples
  l=matrix(nrow=N, ncol=K)  # log likelihood
  p=matrix(nrow=N, ncol=K)  # probability by which each class occurs
  
  # E step
  for(i in 1:N) { 
    for (j in 1:K) {
      # Bernoulli (size = 1): simply multiplying by class probability
      l[i,j] = sum(dbinom(x = data[i,], size = 1, prob = c[j,], log = T)) 
    }
  }
  
  # M step
  for(i in 1:N) {
    p[i,] = q*exp(l[i,]-max(l[i,]))
    p[i,] = p[i,]/sum(p[i,])
  }
  q = colMeans(p)
  # The expected bincounts for each classes are computed at once by means of a matrix multiplication.
  c = (t(p) %*% data)/colSums(p)
  
  # c, q, p are exported for use outside this function. The output variable p is a matrix
  # containing the samples’ posterior probabilities of belonging to particular classes (rows
  # correspond to samples, columns to classes).
  c <<- c; q <<- q; p <<- p;
}
###############################################################################


###################### EM RANDOM SEEDS ########################################
for (K in K_range) {
  print(paste("K", K))
  
  ################################# RUN EM ###############################################
  #
  # Complete algorithm for partitioning with random seeds  
    
  N=dim(data)[1]            # Number of samples
  p=matrix(nrow=N, ncol=K)  # probability by which each class occurs
  
  # set.seed(12)    # same random probabilities each time
  for(i in 1:K) {
    # Samples are randomly assigned probabilities (weights) for each class.
    p[,i] = rbeta(N,N**-0.5,1)
  }
  # these probabilities are used to generate expected bincount vectors for each class.
  # The probabilistic class assignment makes sure that classes will be free of zero values.
  # (Initial zero values are undesirable as they cannot be changed during EM.).
  c = (t(p) %*% data)/colSums(p)
  
  # a vector defining the prior probabilities of each class.
  q=rep(1/K,K)
  
  
  e = convergence_error * length(p) # scale convergence error to size of p
  
  # first iteration
  c[c>1]=1 # dbern error when probability > 1
  p_prev = p
  em_basic(c,q,data)
  p_diff = p_difference(p, p_prev)
  p_diff_prev = 0
  
  i = 2
  while (!p_converged(p_diff, p_diff_prev, e) & i < maxEMIterations) {
    print(paste("i:", i, " , sum:", p_diff, " , step diff:", abs(p_diff-p_diff_prev)))
    c[c>1]=1 # dbern error when probability > 1
    p_prev = p
    p_diff_prev = p_diff
    
    em_basic(c,q,data)
    p_diff = p_difference(p, p_prev)
    
    i = i+1
  }
  ########################################################################################
  
  
  ###################### POST-EM DATA SORTING ############################################
  #
  # assign classes to reads according to the highest class probability
  readsClasses = apply(p, 1, which.max)
  readsTable = table(readsClasses)
  classMeans = aggregate(data, by = list(readsClasses), FUN = mean)[-1]
  
  if (orderByPreviousClusters) {
    print("orderByPreviousClusters")
    # order the classes by comparing the class means to the previous clustering
    classOrder = order_by_prev_cluster(classMeans,firstClusteringClassMeans[[toString(K)]])
    classMeans = classMeans[classOrder, ]
    
  } else {
    print("orderByHClust")
    # order the classes by similarity (class means)
    classOrder = hclust(dist(classMeans))$order
    classMeans = classMeans[classOrder, ]
    
    # for cluster comparison and ordering according to the first clusters
    firstClusteringClassMeans[[toString(K)]] = classMeans
    firstClusteringClassMeans <<- firstClusteringClassMeans
    firstClusteringRepeats <<- 1
  }
  
  ord = order(match(readsClasses,classOrder))
  dataOrderedByClass = data[ord,]
  
  # the horizontal red lines on the plot
  classBorders = head(cumsum(readsTable[classOrder]), -1)
  ########################################################################################
  
  ########################## MAKE PLOTS ##################################################
  # pdfImageMatrix(dataOrderedByClass, "Raw")
  pdfVectorizedReads(dataOrderedByClass, "Raw")
  ########################################################################################
  
  
  if (performSilhouette) {
    silhouetteSingleK()
  }
}
###############################################################################


if (performSilhouette) {
  silhouetteAllKs()
}